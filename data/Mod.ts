interface Mod {
    id: number
    name: string
    description: string
    downloads: number
    follows: number
    author: string
    tags: string[]
}