module.exports = {
    theme: {
      colors: {
        transparent: 'transparent',
        current: 'currentColor',
        'primary': '#047ac4',
        'secondary': '#ca7c0ef1',
        'white': '#ffffff',
        'purple': '#3f3cbb',
        'midnight': '#121063',
        'metal': '#565584',
        'tahiti': '#3ab7bf',
        'silver': '#ecebff',
        'bubble-gum': '#ff77e9',
        'bermuda': '#78dcca',
        'card': '#102c44',
      },
    },
  }