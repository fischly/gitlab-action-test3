FROM node:18-alpine

WORKDIR /app

COPY ./.output/ /app/

# TODO: move to config
ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000

ENTRYPOINT ["node", "/app/server/index.mjs"]