import { describe, expect, it } from 'vitest'
import { setup, $fetch, startServer } from '@nuxt/test-utils'

await setup({
  server: true
})

describe('server api', () => {
  it('should resturn', async () => {
    await startServer();
    let res = await $fetch('/api/basic');
    expect(res).toContain("works")
  })
})
